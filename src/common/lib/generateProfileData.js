import * as data from '../../assets/comments.json';

/**
 * Generate random data to mock api-gotten data
 */
export const generateProfileData = () => {
    return {
        likes: Math.floor(Math.random()*1000),
        followers: Math.floor(Math.random() * 10000),
        following: Math.floor(Math.random()*1000),
        comments: data.default.slice(0, Math.random() * 39 + 10)
    }
};

export const mockUser = {
    name: 'Adam Miauczyński',
    location: 'Warszawa, POL',
};
