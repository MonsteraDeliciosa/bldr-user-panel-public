import React from 'react';
import './Comment.css';

const dummySrc = './avatar.jpg';

const Comment = ({
    author,
    body,
    date
}) => {
    return (
        <li className="comment">
            <div className="commentAvatar"><img className="commentAvatarPic" src={dummySrc} alt="" /></div>
            <div className="commentContainer">
                <p className="commentAuthor">{author}</p>
                <p className="commentBody">{body}</p>
            </div>
            <p className="commentDate">{date > 0 ? `${date} d.` : 'today'}</p>
        </li>
    )
}

export default Comment;