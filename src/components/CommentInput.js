import React from 'react';
import './CommentInput.css';

const CommentInput = ({ onSubmit, onChange, inputValue }) => {

    const onFormSubmit = (e) => {
        onSubmit(e);
        e.target.classList.remove('dirty');
    }

    return (
        <div className="commentInputWrapper">
            <form className={!inputValue.length ? '' : 'dirty'} onSubmit={onFormSubmit}>
                <input className="commentInput" type="text" onChange={onChange} value={inputValue} />
                <label htmlFor="comment" className="commentLabel">Write a comment</label>
            </form>
        </div>
    )
}

export default CommentInput;