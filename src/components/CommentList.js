import './CommentList.css';
import { Comment } from './';
import { CommentInput } from './';
import moment from 'moment';
import React from 'react';

const CommentList = ({
    comments,
    expanded,
    onCommentsToggle,
    onSubmit,
    onChange,
    inputValue,
 }) => {
    const getAgeInDays = (postDate) => moment().diff(moment(postDate), 'days');

    const compare = (a, b) => getAgeInDays(b.posted) - getAgeInDays(a.posted);

    return (
        <div className="commentListContainer">
            <button className="linkButton" onClick={onCommentsToggle}>{expanded ? `Show comments (${comments.length})` : `Hide comments (${comments.length})`}</button>
            <div className={!expanded ? 'commentListShown' : 'commentListHidden'}>
                <ul className="commentList">
                    {comments.sort(compare).map(comment => <Comment key={comment._id} author={`${comment.name.first} ${comment.name.last}`} body={comment.body} date={getAgeInDays(comment.posted)} /> )}
                </ul>
                <CommentInput onSubmit={onSubmit} onChange={onChange} inputValue={inputValue} />
            </div>
        </div>
    )
}

export default CommentList;