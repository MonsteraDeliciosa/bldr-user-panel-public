import './UserProfile.css';
import { CommentList, UserSummary } from './';
import { generateProfileData, mockUser } from './../common/lib/generateProfileData';
import React, { Component } from 'react';

class UserProfile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            comments: generateProfileData().comments,
            commentsHidden: false,
            followers: generateProfileData().followers,
            following: generateProfileData().following,
            inputValue: '',
            likes: generateProfileData().likes,
            likedByUser: null,
            followedByUser: null,
            sharingActive: false,
        }
    }

    onCommentSubmit = (e) => {
        e.preventDefault();

        this.setState({
            comments: [
                ...this.state.comments,
                {
                    _id: Date.now(),
                    name: {
                        first: 'Anonymous',
                        last: 'Anonymous'
                    },
                    posted: Date.now(),
                    body: this.state.inputValue,
                }
            ],
            inputValue: '',
        });
    }

    onInputChange = ({ target: { value } }) => {
        this.setState({ inputValue: value });
    }

    onFollowToggle = () => {
        this.setState({
            followedByUser: !this.state.followedByUser,
            followers: !this.state.followedByUser ? this.state.followers + 1 : this.state.followers - 1,
        });
    }

    onLikeToggle = () => {
        this.setState({
            likedByUser: !this.state.likedByUser,
            likes: !this.state.likedByUser ? this.state.likes + 1 : this.state.likes - 1,
        });
    }

    onCommentsToggle = () => {
        this.setState({ commentsHidden: !this.state.commentsHidden });
    }

    onLinkShare = () => {
        this.setState({ sharingActive: true });
    }

    onShareWindowClose = () => {
        this.setState({ sharingActive: false });
    }

    render() {
        const { comments, inputValue, followedByUser, followers, likedByUser, likes, commentsHidden, following, sharingActive } = this.state;

        return (
            <div className="UserProfile">
                <div className="UserProfileContainer">
                    <UserSummary
                        onFollowClick={this.onFollowToggle}
                        followedByUser={followedByUser}
                        followers={followers}
                        following={following}
                        onLikeClick={this.onLikeToggle}
                        likedByUser={likedByUser}
                        likes={likes}
                        user={mockUser}
                        onLinkShare={this.onLinkShare}
                        onShareWindowClose={this.onShareWindowClose}
                        sharingActive={sharingActive}
                    />
                    <CommentList
                        expanded={commentsHidden}
                        comments={comments}
                        onCommentsToggle={this.onCommentsToggle}
                        onSubmit={this.onCommentSubmit}
                        onChange={this.onInputChange}
                        inputValue={inputValue}
                    />
                </div>
            </div>
        );
    }
}

export default UserProfile;
