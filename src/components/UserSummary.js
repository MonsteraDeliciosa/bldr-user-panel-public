import React from 'react';
import './UserSummary.css';
import { ReactComponent as HeartFull } from './../assets/svg/heart_full.svg';
import { ReactComponent as HeartEmpty } from './../assets/svg/heart_empty.svg';
import { ReactComponent as ExternalLinkIcon } from './../assets/svg/external_link.svg';
import { ReactComponent as CloseIcon } from './../assets/svg/close.svg';

const userImg = './main.jpg';

const UserSummary = ({ onFollowClick, onLikeClick, followedByUser, followers, following, likes, likedByUser, user, sharingActive, onShareWindowClose, onLinkShare }) => {

    const LikeIcon = !!likedByUser ? HeartFull : HeartEmpty;
    const followText = !!followedByUser ? 'unfollow' : 'follow';

    const numbers = [
        {
            number: likes,
            description: 'Likes',
        },
        {
            number: following,
            description: 'Following',
        },
        {
            number: followers,
            description: 'Followers',
        },
    ]

    const renderDescribedNumber = (number, description) => {
        return (
            <li key={number+description} className="numbersItem">
                <span className="numbersItemAmount">{number}</span>
                <span className="numbersItemDescription">{description}</span>
            </li>
        )
    }

    const onLinkCopy = () => {
        const input = document.querySelector('.shareInput');

        input.select();
        document.execCommand("copy");
    }

    return (
        <div className="userSummary">
            <button className="shareUser" onClick={onLinkShare} title="share"><ExternalLinkIcon className="shareUserIcon" /></button>
            {sharingActive &&
                <div className="shareWindow">
                    <button className="shareCloseBtn" onClick={onShareWindowClose} title="share"><CloseIcon className="closeIcon" /></button>
                    <button className="shareInfoText" onClick={onLinkCopy}>Copy link to clipboard:</button>
                    <input className="shareInput" type="text" readOnly value={window.location} />
                </div>
            }
            <div className="userContainer">
                <div className="userAvatar">
                    <img src={userImg} alt="" />
                </div>
                <div className="userInfo">
                    <div className="nameRow">
                        <span className="userName">{user.name}</span>
                        <button className="likeButton" onClick={onLikeClick} title={!!likedByUser ? 'dislike user' : 'like user'}><LikeIcon className="heartIcon" /></button>
                    </div>
                    <span className="location">{user.location}</span>
                </div>
            </div>
            <div className="numbersContainer">
                <ul className="numbersList">
                    {numbers.map(element => renderDescribedNumber(element.number, element.description))}
                </ul>
                <button className="followButton" onClick={onFollowClick}>{followText}</button>
            </div>

        </div>
    )
}

export default UserSummary;